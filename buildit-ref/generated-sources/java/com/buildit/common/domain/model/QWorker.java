package com.buildit.common.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QWorker is a Querydsl query type for Worker
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QWorker extends BeanPath<Worker> {

    private static final long serialVersionUID = 2051277036L;

    public static final QWorker worker = new QWorker("worker");

    public final StringPath name = createString("name");

    public final EnumPath<ROLEStatus> role = createEnum("role", ROLEStatus.class);

    public final NumberPath<Long> worker_id = createNumber("worker_id", Long.class);

    public QWorker(String variable) {
        super(Worker.class, forVariable(variable));
    }

    public QWorker(Path<? extends Worker> path) {
        super(path.getType(), path.getMetadata());
    }

    public QWorker(PathMetadata<?> metadata) {
        super(Worker.class, metadata);
    }

}

