package com.buildit.hire.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QInvoice is a Querydsl query type for Invoice
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QInvoice extends EntityPathBase<Invoice> {

    private static final long serialVersionUID = -727542378L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QInvoice invoice = new QInvoice("invoice");

    public final QInvoiceID id;

    public final EnumPath<INType> invoiceType = createEnum("invoiceType", INType.class);

    public final QPurchaseOrder orderInfo;

    public final DateTimePath<java.util.Date> paymentSchedule = createDateTime("paymentSchedule", java.util.Date.class);

    public final EnumPath<INStatus> status = createEnum("status", INStatus.class);

    public QInvoice(String variable) {
        this(Invoice.class, forVariable(variable), INITS);
    }

    public QInvoice(Path<? extends Invoice> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoice(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QInvoice(PathMetadata<?> metadata, PathInits inits) {
        this(Invoice.class, metadata, inits);
    }

    public QInvoice(Class<? extends Invoice> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QInvoiceID(forProperty("id")) : null;
        this.orderInfo = inits.isInitialized("orderInfo") ? new QPurchaseOrder(forProperty("orderInfo"), inits.get("orderInfo")) : null;
    }

}

