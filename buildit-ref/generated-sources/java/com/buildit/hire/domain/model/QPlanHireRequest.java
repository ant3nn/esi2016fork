package com.buildit.hire.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QPlanHireRequest is a Querydsl query type for PlanHireRequest
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QPlanHireRequest extends BeanPath<PlanHireRequest> {

    private static final long serialVersionUID = -1874902149L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlanHireRequest planHireRequest = new QPlanHireRequest("planHireRequest");

    public final StringPath comment = createString("comment");

    public final NumberPath<Long> construction_site_id = createNumber("construction_site_id", Long.class);

    public final NumberPath<Float> cost = createNumber("cost", Float.class);

    public final com.buildit.common.domain.model.QBusinessPeriod period;

    public final QPlantHireRequestID phr_id;

    public final SimplePath<java.net.URL> plant_id = createSimple("plant_id", java.net.URL.class);

    public final com.buildit.common.domain.model.QWorker site_engineer;

    public final EnumPath<PHRStatus> status = createEnum("status", PHRStatus.class);

    public final StringPath supplier = createString("supplier");

    public final com.buildit.common.domain.model.QWorker work_engineer;

    public QPlanHireRequest(String variable) {
        this(PlanHireRequest.class, forVariable(variable), INITS);
    }

    public QPlanHireRequest(Path<? extends PlanHireRequest> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlanHireRequest(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QPlanHireRequest(PathMetadata<?> metadata, PathInits inits) {
        this(PlanHireRequest.class, metadata, inits);
    }

    public QPlanHireRequest(Class<? extends PlanHireRequest> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.period = inits.isInitialized("period") ? new com.buildit.common.domain.model.QBusinessPeriod(forProperty("period")) : null;
        this.phr_id = inits.isInitialized("phr_id") ? new QPlantHireRequestID(forProperty("phr_id")) : null;
        this.site_engineer = inits.isInitialized("site_engineer") ? new com.buildit.common.domain.model.QWorker(forProperty("site_engineer")) : null;
        this.work_engineer = inits.isInitialized("work_engineer") ? new com.buildit.common.domain.model.QWorker(forProperty("work_engineer")) : null;
    }

}

