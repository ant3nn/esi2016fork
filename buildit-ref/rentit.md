FORMAT: 1A
//HOST: http://polls.apiblueprint.org/

# BuildIT

BuildIT allows construction company to query available plants from 
several rental companies and create a purchase order and review all the invoices accepted 
from other systems. 

## Plants [/plants/{retailer_name}]

### List All Plants [GET]

Engineer can query all the Plants in the BuiltIT system.

+ Parameters
    + retailer_name (string) - retailer company who provides plants

+ Response 200 (application/json)

        [
            {   
                "url": "/plants/rentit",
                "plants": [
                    {
                        "plant_url": "/plants/rentit/1"
                    }, {
                        "plant_url": "plants/rentit/2"
                    }, {
                        "plant_url": "plants/rentit/3"
                    }, {
                        "plant_url": "plants/rentit/4"
                    }, {
                        "plant_url": "plants/rentit/5"
                    }, {
                        "plant_url": "plants/rentit/6"
                    }, {
                        "plant_url": "plants/rentit/7"
                    }, {
                        "plant_url": "plants/rentit/8"
                    }, {
                        "plant_url": "plants/rentit/9"
                    }
                ]
            }
        ]

+ Response 404 (text/plain)
    
        Nothing to show

## Plant [/plant/{id}]

### Plant data [GET]

Engineer can query data of a Plant in BuiltIT system.

+ parameters 
    + id (number) - plant ID
    
+ Response 200 (application/json)

        {
            "plant_url": "/plants/rentit/1",
            "name": "Mini Excavator",
            "description":"1.5 Tonne Mini excavator",
            "price": 150 
        }

## PlantHireRequests [/plant-hire-request]

### List All PHR-s [GET]

Work engineer can query all Plant Hire Requests on BuildIT system.

+ Response 200 (application/json)

        [
            {   
                "url": "/plant-hire_request",
                "plant hire requests": [
                    {
                        "phr_url": "/plant-hire_request/123"
                    }, {
                        "phr_url": "/plant-hire_request/126"
                    }, {
                        "phr_url": "/plant-hire_request/129"
                    }, {
                        "phr_url": "/plant-hire_request/221"
                    }, {
                        "phr_url": "/plant-hire_request/321"
                    }, {
                        "phr_url": "/plant-hire_request/2436"
                    }, {
                        "phr_url": "/plant-hire_request/21"
                    }, {
                        "phr_url": "/plant-hire_request/1231"
                    }, {
                        "phr_url": "/plant-hire_request/88"
                    }
                ]
            }
        ]

+ Response 404 (text/plain)
    
        Nothing to show

### Create new PHR [POST]

Site engineer can create a Plant Hire Request in the BuiltIT system.

+ Request (application/json)

        {
            "construction_site_id": 2178,
            "plant_id": 9,
            "supplier": "RentIT",
            "cost": 200,
            "site_engineer": 
            {
                "worker":
                {
                    "id": 1,
                    "role": "SITE_ENGINEER",
                    "name": "Annett"
                }
            },
            "work_engineer":
            {
                "worker":
                {
                    "id": 2,
                    "role": "WORK_ENGINEER",
                    "name": "Umesh"
                }
            },
            "status": "PENDING",
            "period": 
            {
                "businessperiod":
                {
                    "start_date": "2016-03-03,
                    "end_date": "2016-12-12"
                }
            },
            "comment": ""
        }

+ Response 201 (application/json)

    + Headers

            Location: /plant-hire-requests/123

## PlantHireRequest [/plant-hire-request/{id}]

### Modify PHR [PUT]

Engineer can modify Plant Hire Request in the BuiltIT system.

+ Parameters
    + id (number) - Plant Hire Request ID
    
+ Request (application/json)

        {
            "construction_site_id": 2178,
            "plant_id": 9,
            "supplier": "RentIT",
            "cost": 200,
            "site_engineer": 
            {
                "worker":
                {
                    "id": 1,
                    "role": "SITE_ENGINEER",
                    "name": "Annett"
                }
            },
            "work_engineer":
            {
                "worker":
                {
                    "id": 2,
                    "role": "WORK_ENGINEER",
                    "name": "Umesh"
                }
            },
            "status": "ACCEPTED",
            "period": 
            {
                "businessperiod":
                {
                    "start_date": "2016-03-03,
                    "end_date": "2016-12-12"
                }
            },
            "comment": "I will accept this phr, but try to lower costs"
        }

+ Response 204 (application/json)


## Invoices [/invoices/rental]

### List all invoices [GET]

The engineer can query all the Invoices in the BuiltIT system.

+ Response 200 (application/json)

        [
            {   
                "url": "/invoice_list",
                "invoices": [
                    {
                        "invoice_url": "/invoices/rental/rentit/1"
                    }, {
                        "invoice_url": "invoices/rental/rentit/2"
                    }, {
                        "invoice_url": "invoices/rental/rentit/3"
                    }, {
                        "invoice_url": "invoices/rental/rentit/4"
                    }, {
                        "invoice_url": "invoices/rental/rentit/5"
                    }
                ]
            }
        ]
            
## Invoice [/invoice/{id}]
            
### Invoice data [GET]

Engineer can query data of an Invoice in BuiltIT system.

+ parameters 
    + id (number) - invoice ID
    
+ Response 200 (application/json)

        {
            "invoice_url": "invoices/rental/rentit/5",
            "purchace_order":
                {
                    "PurchaseOrder" :
                        {
                            "po_id" : 12657976
                        }
                },
            "status": "MODIFIED",
            "payment_schedule": "13.04.2016",
        }
            
### Accept an invoice [PUT] 

The engineer can accept an Invoice in the BuiltIT system.

+ Parameters
    
    + id (number) - Invoice ID
    
+ Request (application/json)

    
        {
            "invoice_url": "invoices/rental/rentit/5",
            "purchace_order":
                {
                    "PurchaseOrder" :
                        {
                            "po_id" : 12657976
                        }
                },
            "status": "ACCEPTED",
            "payment_schedule": "13.04.2016",
        }
    
+ Response 200 (application/json)

## PurchaseOrder [/purchase-order]

### List All PurchaseOrders [GET]

Engineer can query all Purchase Orders in BuiltIT system.

+ Response 200 (application/json)

        [      
            {   
                "url": "/purchase-order"
                "purchaseorders": [ 
                    {
                        "po_url": "/purchase-order/12657976",
                        "phr_url": "plant-hire-request/123",
                        "status":"ACCEPTED",
                        "period": 
                            {
                                "businessperiod":
                                    {
                                        "start_date": "2016-03-03,
                                        "end_date": "2016-12-12",
                                    }
                            },
                       "cost": 150,
                    }
                ]
            }
        ]
