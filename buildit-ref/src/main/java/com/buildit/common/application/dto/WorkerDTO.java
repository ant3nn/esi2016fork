package com.buildit.common.application.dto;

import com.buildit.common.domain.model.ROLEStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class WorkerDTO {
    Long worker_id;
    ROLEStatus role;
    String name;
}
