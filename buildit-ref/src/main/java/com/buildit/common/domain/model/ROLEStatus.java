package com.buildit.common.domain.model;

public enum ROLEStatus {
    SITE_ENGINEER, WORK_ENGINEER
}
