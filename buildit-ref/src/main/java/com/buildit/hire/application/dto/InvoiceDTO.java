package com.buildit.hire.application.dto;


import com.buildit.hire.domain.model.INStatus;
import com.buildit.hire.domain.model.INType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;


@Getter
@Setter
@NoArgsConstructor
public class InvoiceDTO extends ResourceSupport {
    Long _id;
    PurchaseOrderDTO po;
    INType invoiceType;
    INStatus status;
}
