package com.buildit.hire.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.application.dto.WorkerDTO;
import com.buildit.hire.domain.model.PHRStatus;
import com.buildit.hire.domain.model.PlantHireRequestID;
import java.net.URL;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class PlantHireRequestDTO {
    PlantHireRequestID phr_id;
    Long construction_site_id;
    URL plant_id;
    String supplier;
    float cost;
    WorkerDTO site_engineer;
    WorkerDTO work_engineer;
    PHRStatus status;
    BusinessPeriodDTO period;
    String comment;
    private Long id;

    public Long getId() {
        return id;
    }
}
