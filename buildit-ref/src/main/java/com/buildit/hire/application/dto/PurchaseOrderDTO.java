package com.buildit.hire.application.dto;


import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.hire.domain.model.POStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;


@Getter
@Setter
@NoArgsConstructor
public class PurchaseOrderDTO extends ResourceSupport {
    Long _id;
    BusinessPeriodDTO schedule;
    PlantHireRequestDTO hire;
    POStatus status;
}
