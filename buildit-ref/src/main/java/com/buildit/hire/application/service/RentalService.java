package com.buildit.hire.application.service;

import com.buildit.hire.application.dto.PlantHireRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class RentalService {
    @Autowired
    RestTemplate restTemplate;
    public List<PlantHireRequestDTO> findPlantHireRequest(Long phr_id) {
        PlantHireRequestDTO[] phrList = restTemplate.getForObject(
                "http://localhost:8090/api/planthirerequest/phr?id={phr_id}",
                PlantHireRequestDTO[].class, phr_id);
        return Arrays.asList(phrList);
    }

    public List<PlantHireRequestDTO> findPlantHireRequests() {
        PlantHireRequestDTO[] phrList = restTemplate.getForObject(
                "http://localhost:8090/api/planthirerequests/",
                PlantHireRequestDTO[].class);
        return Arrays.asList(phrList);

    }

    public PlantHireRequestDTO createPlantHireRequest() {
        PlantHireRequestDTO phr  = restTemplate.getForObject(
                "http://localhost:8090/api/planthirerequest/",
                PlantHireRequestDTO.class);
        return phr;
    }
}


