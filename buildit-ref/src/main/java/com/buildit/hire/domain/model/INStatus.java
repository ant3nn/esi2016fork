package com.buildit.hire.domain.model;



public enum INStatus {
    PENDING, ACCEPTED, REJECTED
}
