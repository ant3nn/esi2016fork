package com.buildit.hire.domain.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Darwintartu on 05-Apr-16.
 */

@Entity
@Getter @ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class Invoice  {
    @EmbeddedId
    InvoiceID id;
    @Embedded
    PurchaseOrder orderInfo;



    Date paymentSchedule;

    @Enumerated(EnumType.STRING)
    INType invoiceType;
    INStatus status;

}