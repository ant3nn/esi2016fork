package com.buildit.hire.domain.model;


public enum PHRStatus {
    REJECTED, ACCEPTED, PENDING
}
