package com.buildit.hire.domain.model;



public enum POStatus {
    PENDING, ACCEPTED, REJECTED, COMPLETED
}
