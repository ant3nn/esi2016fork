package com.buildit.hire.domain.model;

import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.common.domain.model.Worker;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.net.URL;


@Embeddable
@Value
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class PlantHireRequest {
    PlantHireRequestID phr_id;
    Long construction_site_id;
    URL plant_id;
    String supplier;
    float cost;
    Worker site_engineer;
    Worker work_engineer;
    PHRStatus status;
    BusinessPeriod period;
    String comment;

}