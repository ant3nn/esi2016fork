package com.buildit.hire.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter @ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class PurchaseOrder {
    @EmbeddedId
    PurchaseOrderID id;
    //PlantHireRequest;

    @Embedded
    PlantHireRequestID hireInfo;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Column(precision = 8, scale = 2)
    BigDecimal cost;
}
