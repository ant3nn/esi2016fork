package com.buildit.hire.rest;

import com.buildit.hire.application.dto.PlantHireRequestDTO;
import com.buildit.hire.application.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/hires")
public class PlantHireRequestRestController {
    @Autowired
    RentalService rentalService;

    @RequestMapping(method = RequestMethod.GET, path = "/hires")
    public List<PlantHireRequestDTO> findAllPlantHireRequests(

            //@RequestParam(name = "name") String name,
            //@RequestParam(name = "cost") float cost,
            //@RequestParam(name = "id") PlantHireRequestID id
    ) {
        return rentalService.findPlantHireRequests();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hires/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PlantHireRequestDTO> fetchPlantHireRequest(@PathVariable("id") Long id) {

        return rentalService.findPlantHireRequest(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/hires")
    public ResponseEntity<PlantHireRequestDTO> createPlantHireRequest(@RequestBody PlantHireRequestDTO partialPHRDTO) throws URISyntaxException {
         PlantHireRequestDTO newlyCreatePHRDTO = rentalService.createPlantHireRequest();

        HttpHeaders headers = new HttpHeaders();
        try {
            headers.setLocation(new URI(newlyCreatePHRDTO.getId().toString()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<PlantHireRequestDTO>(newlyCreatePHRDTO, headers, HttpStatus.CREATED);
    }
}



