package com.buildit;

import com.buildit.hire.application.service.RentalService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by Umesh on 06/04/2016.
 */
@Configuration
@Profile("test")
public class TestContext {
    @Bean
    public RentalService rentalService() {
        return Mockito.mock(RentalService.class);
    }
}
