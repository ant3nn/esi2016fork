package com.rentit.common.domain.entities;

import org.joda.time.LocalDate;


import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;;

@Embeddable
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
public class BusinessPeriod {

	LocalDate start_date;
	LocalDate end_date;

}