package com.rentit.common.domain.validators;

import org.joda.time.LocalDate;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.rentit.common.domain.entities.BusinessPeriod;

public class PeriodValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		
		BusinessPeriod bp = (BusinessPeriod) object;
		
		if(bp.getStart_date() == null || bp.getEnd_date() == null) {
			errors.rejectValue("rental_period", "Rental period cannot be null");
		}
		if(bp.getEnd_date().isBefore(bp.getStart_date())) {
			errors.rejectValue("rental_period", "end_date cannot be before start_date");
		}
		if(isBeforeNow(bp.getStart_date())) {
			errors.rejectValue("rental_period", "start_date cannot be before current date");
		}
		if(isBeforeNow(bp.getEnd_date())) {
			errors.rejectValue("rental_period", "end_date cannot be before current date");
		}
	}
	
	public boolean isBeforeNow(LocalDate inputDate) {
		
		LocalDate now = new LocalDate(System.currentTimeMillis());
		
		int day = now.getDayOfMonth();
		int month = now.getMonthOfYear(); 
		int year = now.getYear();
		
		if(!isToday(inputDate)) {
			if(inputDate.getYear() < year) {
				return true;
			}
			else if(inputDate.getMonthOfYear() < month) {
				return true;
			}
			else if(inputDate.getDayOfMonth() < day) {
				return true;
			}
			else {
				return true;
			}
		}
		else {
			if(isAfterNow(inputDate)) {
				return false;
			}
			else {
				return true;
			}
			
		}
		
	}
	
	public boolean isAfterNow(LocalDate inputDate) {
		
		LocalDate now = new LocalDate(System.currentTimeMillis());
		
		int day = now.getDayOfMonth();
		int month = now.getMonthOfYear(); 
		int year = now.getYear();
		
		if(!isToday(inputDate)) {
			if(inputDate.getYear() > year) {
				return true;
			}
			else if(inputDate.getMonthOfYear() > month) {
				return true;
			}
			else if(inputDate.getDayOfMonth() > day) {
				return true;
			}
			else {
				return true;
			}
		}
		else {
			if(isBeforeNow(inputDate)) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	
	public boolean isToday(LocalDate inputDate) {
		
		org.joda.time.LocalDate now = new LocalDate(System.currentTimeMillis());
		
		int day = now.getDayOfMonth();
		int month = now.getMonthOfYear(); 
		int year = now.getYear();
		
		if(inputDate.getYear() == year) {
			if(inputDate.getMonthOfYear() == month) {
				if(inputDate.getDayOfMonth() == day) {
					return true;
				}
			}
		}
		
		return false;
	}

}
