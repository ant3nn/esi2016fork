package com.rentit.common.web.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import com.rentit.common.domain.entities.BusinessPeriod;

import org.joda.time.LocalDate;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Data
public class BusinessPeriodDTO {
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate;

    // Return the number of days between start_date and end_date
    public BusinessPeriod asBusinessPeriod() {

        return BusinessPeriod.of(this.startDate, this.endDate);
    }
    
    // Returns the number of days between start_date and end_date
    // Reference - http://stackoverflow.com/questions/20165564/calculating-days-between-two-dates-with-in-java
    public static long getDifferenceInDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.DAYS);
    }
    
}