package com.rentit.customer.domain.entities;

import com.rentit.customer.domain.identifiers.CustomerID;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by Umesh on 11/03/2016.
 */
@Entity
@Data
public class Customer {
    @Id
    @EmbeddedId
    CustomerID id;

    String name;
    @OneToOne
    ContactPerson contactPerson;
    String company;
}
