package com.rentit.customer.domain.identifiers;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Umesh on 11/03/2016.
 */
@Embeddable
@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class ContactPersonID implements Serializable {
    Long id;
}