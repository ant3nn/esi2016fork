package com.rentit.inventory.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rentit.common.infrastructure.HibernateBasedIdentifierGenerator;
import com.rentit.inventory.domain.identifiers.PlantInventoryEntryID;


@Service
public class InventoryIdentifierFactory {

	@Autowired
    HibernateBasedIdentifierGenerator idGenerator;

    public PlantInventoryEntryID nextPlantInventoryEntryID() {
        return PlantInventoryEntryID.of(idGenerator.getID("PlantInventoryEntryIDSequence"));
    }
	
}
