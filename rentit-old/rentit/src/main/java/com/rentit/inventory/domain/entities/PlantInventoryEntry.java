package com.rentit.inventory.domain.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.rentit.inventory.domain.identifiers.PlantInventoryEntryID;

import lombok.Data;

@Entity
@Data
public class PlantInventoryEntry {

	@Id @EmbeddedId
	PlantInventoryEntryID id;
	
	String name;
	String description;
	
	@Column(precision=8, scale=2)
	BigDecimal price;
	
	/*
	 * FACTORY METHOD ON CLASS
	 */
	
	protected PlantInventoryEntry() {}
	
	public static PlantInventoryEntry of(PlantInventoryEntryID id, String arg1, 
			String arg2, BigDecimal arg3) {
		
		PlantInventoryEntry plant = new PlantInventoryEntry();
		plant.id = id;
		plant.name = arg1;
		plant.description = arg2;
		plant.price = arg3;
		
		return plant;
	}

}
