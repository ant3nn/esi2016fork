package com.rentit.inventory.domain.entities;

import javax.persistence.EmbeddedId;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.rentit.inventory.domain.enumerations.*;
import com.rentit.inventory.domain.identifiers.PlantInventoryItemID;

import lombok.Data;

@Entity
@Data
public class PlantInventoryItem {

	@Id
	@EmbeddedId
	PlantInventoryItemID id;

	String serial_number;

	@Enumerated(EnumType.STRING)
	EquipmentCondition condition;
	
	/*
	 * FACTORY METHOD ON CLASS
	 */
	
	protected PlantInventoryItem() {}
	
	public static PlantInventoryItem of(PlantInventoryItemID id, String arg1, EquipmentCondition arg2) {
		
		PlantInventoryItem item = new PlantInventoryItem();
		item.id = id;
		item.serial_number = arg1;
		item.condition = arg2;
		
		return item;
	}
}
