package com.rentit.inventory.domain.entities;

import javax.persistence.*;

import com.rentit.common.domain.entities.BusinessPeriod;
import com.rentit.inventory.domain.identifiers.PlantReservationID;

import lombok.Data;

@Entity
@Data
public class PlantReservation {
	
	@Id
	PlantReservationID id;
	
	@Embedded
	BusinessPeriod rental_period;
	
	/*
	 * FACTORY METHOD
	 */
	protected PlantReservation() {}
	
	public static PlantReservation of(PlantReservationID id, BusinessPeriod arg1) {
		
		PlantReservation reservation = new PlantReservation();
		reservation.id = id;
		reservation.rental_period = arg1;
		
		return reservation;
	}
	
}
