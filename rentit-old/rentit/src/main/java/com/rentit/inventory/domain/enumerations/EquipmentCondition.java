package com.rentit.inventory.domain.enumerations;

public enum EquipmentCondition {
 SERVICEABLE, UNSERVICEABLEREPAIRABLE, UNSERVICEABLEINCOMPLETE, UNSERVICEABLECONDEMNED    
}
