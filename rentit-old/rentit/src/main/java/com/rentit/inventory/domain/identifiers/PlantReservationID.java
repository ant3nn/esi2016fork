package com.rentit.inventory.domain.identifiers;

import java.io.Serializable;


import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;


@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class PlantReservationID implements Serializable {

	Long Id;
}
