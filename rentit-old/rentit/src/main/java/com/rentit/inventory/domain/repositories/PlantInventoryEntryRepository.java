package com.rentit.inventory.domain.repositories;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rentit.inventory.domain.identifiers.PlantInventoryEntryID;
import com.rentit.inventory.domain.entities.PlantInventoryEntry;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, PlantInventoryEntryID> {

	List<PlantInventoryEntry> findByNameContaining(String str);

	@Query("select p from PlantInventoryEntry p where LOWER(p.name) like CONCAT ('%', LOWER(?1),'%')")
	List<PlantInventoryEntry> finderMethod(String name);
	
	//TODO
	@Query("select p from PlantInventoryEntry p, PlantReservation pr where LOWER(p.name) like CONCAT ('%', LOWER(?1),'%') OR pr.rental_period.start_date >= ?2 AND pr.rental_period.end_date >= ?3 ")
	List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate start_date, LocalDate end_date);
	
	@Query("select p from PlantInventoryEntry p where p.id = ?1")
	PlantInventoryEntry selectPlantInventoryEntry(PlantInventoryEntryID id);
}
