package com.rentit.inventory.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rentit.inventory.domain.entities.PlantInventoryItem;

public interface PlantInventoryItemRepository  extends JpaRepository<PlantInventoryItem, Long>
{

}
