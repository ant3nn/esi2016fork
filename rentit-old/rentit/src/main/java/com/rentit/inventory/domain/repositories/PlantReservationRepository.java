package com.rentit.inventory.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rentit.inventory.domain.entities.PlantReservation;

public interface PlantReservationRepository extends JpaRepository<PlantReservation, Long>{

	
	
}
