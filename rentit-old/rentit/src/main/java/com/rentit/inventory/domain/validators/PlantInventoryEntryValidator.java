package com.rentit.inventory.domain.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.rentit.inventory.domain.entities.PlantInventoryEntry;

public class PlantInventoryEntryValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		
		PlantInventoryEntry pie = (PlantInventoryEntry) object;
		
		if(pie.getId() == null) {
			errors.rejectValue("Id", "PlantInventoryEntry Id cannot be null");
		}
		if(pie.getName() == null) {
			errors.rejectValue("name", "PlantInventoryEntry name cannot be null");
		}
		if(pie.getDescription() == null) {
			errors.rejectValue("description", "Please fill description for PlantInventoryEntry");
		}
		if(pie.getPrice().intValue() <= 0) {
			errors.rejectValue("price", "Price cannot be 0 or negative");
		}
		
	}

}
