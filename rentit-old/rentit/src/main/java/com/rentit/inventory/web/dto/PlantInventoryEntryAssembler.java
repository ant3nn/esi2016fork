package com.rentit.inventory.web.dto;
import com.rentit.inventory.web.InventoryRESTController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import org.springframework.stereotype.Service;
import com.rentit.inventory.domain.entities.PlantInventoryEntry;

@Service
public class PlantInventoryEntryAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> { 

	  public PlantInventoryEntryAssembler() {
	    super(InventoryRESTController.class, PlantInventoryEntryDTO.class);
	  }

	  @Override
	  public PlantInventoryEntryDTO toResource(PlantInventoryEntry plant) {
		  PlantInventoryEntryDTO dto = createResourceWithId(plant.getId().getId(), plant); 
		  dto.set_id(plant.getId().getId());
		  dto.setName(plant.getName());
		  dto.setDescription(plant.getDescription());
		  dto.setPrice(plant.getPrice());
	    
		  return dto;
	  } 

}
