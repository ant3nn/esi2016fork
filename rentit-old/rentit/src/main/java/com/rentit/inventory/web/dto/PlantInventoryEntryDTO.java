package com.rentit.inventory.web.dto;

import java.math.BigDecimal;
import org.springframework.hateoas.ResourceSupport;
import lombok.Data;

@Data
public class PlantInventoryEntryDTO extends ResourceSupport {
	
	private Long _id;
	private String name;
	private String description;
	private BigDecimal price;

}
