package com.rentit.maintenance.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rentit.common.infrastructure.HibernateBasedIdentifierGenerator;
import com.rentit.maintenance.domain.identifiers.MaintenancePlanID;
import com.rentit.maintenance.domain.identifiers.MaintenanceTaskID;

@Service
public class MaintenanceIdentifierFactory {

	@Autowired
    HibernateBasedIdentifierGenerator idGenerator;

    public MaintenancePlanID nextMaintenancePlanID() {
        return MaintenancePlanID.of(idGenerator.getID("MaintenancePlanIDSequence"));
    }
    
    public MaintenanceTaskID nextMaintenanceTaskID() {
        return MaintenanceTaskID.of(idGenerator.getID("MaintenanceTaskIDSequence"));
    }
}
