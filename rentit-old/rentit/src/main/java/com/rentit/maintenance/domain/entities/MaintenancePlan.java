package com.rentit.maintenance.domain.entities;

import javax.persistence.*;
import com.rentit.maintenance.domain.identifiers.MaintenancePlanID;

import lombok.Data;

@Entity
@Data
public class MaintenancePlan {
	
	@Id @EmbeddedId
	MaintenancePlanID id;
	
	Integer year_of_action;
	
	/*
	 * FACTORY METHOD ON CLASS
	 */
	protected MaintenancePlan() {}
	
	public static MaintenancePlan of(MaintenancePlanID id, Integer arg1) {
		MaintenancePlan mp = new MaintenancePlan();
		mp.id = id;
		mp.year_of_action = arg1;
		
		return mp;
	}
}
