package com.rentit.maintenance.domain.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.rentit.common.domain.entities.BusinessPeriod;
import com.rentit.maintenance.domain.enumerations.TypeOfWork;
import com.rentit.maintenance.domain.identifiers.MaintenanceTaskID;

import lombok.Data;

@Entity
@Data
public class MaintenanceTask {

	@Id @EmbeddedId
	MaintenanceTaskID id;

    String description;
	
	@Enumerated(EnumType.STRING)
	TypeOfWork work;
	
	@Column(precision=8, scale=2)
	BigDecimal price;
	
	@Embedded
	BusinessPeriod schedule;
	
	/*
	 * FACTORY METHOD ON CLASS
	 */
	protected MaintenanceTask() {}
	
	public static MaintenanceTask of(MaintenanceTaskID id, String arg1, TypeOfWork arg2, 
			BigDecimal arg3, BusinessPeriod arg4) {
		
		MaintenanceTask mt = new MaintenanceTask();
		mt.id = id;
		mt.description = arg1;
		mt.work = arg2;
		mt.price = arg3;
		mt.schedule = arg4;
		
		return mt;
	}
}
