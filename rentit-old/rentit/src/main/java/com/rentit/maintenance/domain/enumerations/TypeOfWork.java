package com.rentit.maintenance.domain.enumerations;

public enum TypeOfWork {
PREVENTIVE, CORRECTIVE, OPERATIVE
}
