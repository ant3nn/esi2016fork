package com.rentit.maintenance.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rentit.maintenance.domain.entities.MaintenancePlan;

public interface MaintenancePlanRepository extends JpaRepository<MaintenancePlan, Long> {

}
