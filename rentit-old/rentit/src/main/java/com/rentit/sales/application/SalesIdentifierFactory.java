package com.rentit.sales.application;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rentit.common.infrastructure.HibernateBasedIdentifierGenerator;
import com.rentit.inventory.domain.repositories.PlantInventoryEntryRepository;
import com.rentit.sales.domain.identifiers.PurchaseOrderID;
import com.rentit.sales.domain.repositories.PurchaseOrderRepository;

@Service
public class SalesIdentifierFactory {

	@Autowired
    HibernateBasedIdentifierGenerator idGenerator;
	
	public PurchaseOrderID nextPurchaseOrderID() {
        return PurchaseOrderID.of(idGenerator.getID("PurchaseOrderIDSequence"));
    }
	
	@Autowired
	PurchaseOrderRepository orderRepo;

	@Autowired
	PlantInventoryEntryRepository plantRepo;

	// @Autowired
	// IdentifierFactory idFactory;
	//
	// public void createPO(PlantInventoryEntryDTO plantDTO, BusinessPeriodDTO
	// periodDTO) {
	// PurchaseOrder po = PurchaseOrder.of(idFactory.nextPurchaseOrderID(),
	// PlantInventoryEntryID.of(plantDTO.getId()),
	// periodDTO.asBusinessPeriod());
	// DataBinder binder = new DataBinder(po);
	// binder.addValidators(new PurchaseOrderValidator());
	// binder.validate();
	// if (!binder.getBindingResult().hasErrors())
	// orderRepo.save(po);
	// }
}
