package com.rentit.sales.application;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import com.rentit.common.web.dto.BusinessPeriodDTO;
import com.rentit.inventory.application.InventoryService;
import com.rentit.inventory.domain.identifiers.PlantInventoryEntryID;
import com.rentit.inventory.domain.identifiers.PlantReservationID;
import com.rentit.inventory.domain.repositories.PlantInventoryEntryRepository;
import com.rentit.inventory.web.dto.PlantInventoryEntryDTO;
import com.rentit.sales.domain.identifiers.PurchaseOrderID;
import com.rentit.sales.domain.entities.PurchaseOrder;
import com.rentit.sales.domain.repositories.PurchaseOrderRepository;
import com.rentit.sales.domain.validators.PurchaseOrderValidator;

@Service
public class SalesService {
	
	@Autowired
	InventoryService inventory_service;
	@Autowired
	PlantInventoryEntryRepository plant_inventory_entry_repository;
	@Autowired
	PurchaseOrderRepository purchase_order_repository;
	@Autowired
	SalesIdentifierFactory id_factory;
	
	public void createPO(PlantInventoryEntryDTO plant_inventory_entry_dto, BusinessPeriodDTO business_period_dto) { 
		
		PurchaseOrderID id = id_factory.nextPurchaseOrderID();
		PlantReservationID plant_reservation_id = purchase_order_repository.selectPlantInventoryEntry(id).getPlant_reservation_id();
					
		PurchaseOrder po = PurchaseOrder.of(id, plant_reservation_id, PlantInventoryEntryID.of(plant_inventory_entry_dto.get_id()),plant_inventory_entry_dto.getPrice(), business_period_dto.asBusinessPeriod());
		DataBinder binder = new DataBinder(po); 
		binder.addValidators(new PurchaseOrderValidator()); 
		binder.validate();
		if (!binder.getBindingResult().hasErrors()) {
			purchase_order_repository.save(po);
		}		
	}
}
