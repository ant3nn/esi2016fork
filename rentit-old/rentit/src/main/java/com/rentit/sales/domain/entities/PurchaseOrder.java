package com.rentit.sales.domain.entities;

import java.math.BigDecimal;

import javax.persistence.*;

import com.rentit.common.domain.entities.BusinessPeriod;
import com.rentit.inventory.domain.identifiers.PlantInventoryEntryID;
import com.rentit.inventory.domain.identifiers.PlantReservationID;
import com.rentit.sales.domain.enumerations.POStatus;
import com.rentit.sales.domain.identifiers.PurchaseOrderID;

import lombok.Data;

@Entity
@Data
public class PurchaseOrder {

	@Id @EmbeddedId
	PurchaseOrderID id;   
	
	PlantInventoryEntryID plant_inventory_entry_id;
	PlantReservationID plant_reservation_id;
		
	@Column(precision = 8, scale = 2)
	BigDecimal price;

	@Enumerated(EnumType.STRING)
	POStatus status;

	@Embedded
	BusinessPeriod rental_period;
	
	/*
	 * FACTORY METHOD ON CLASS
	 */
	protected PurchaseOrder() {}
	
	public static PurchaseOrder of(
			PurchaseOrderID id, 
			PlantReservationID arg1, 
			PlantInventoryEntryID arg2, 
			BigDecimal arg3, 
			BusinessPeriod arg4) {
			
		PurchaseOrder po = new PurchaseOrder();
		
		po.id = id;
		po.plant_reservation_id = arg1;
		po.plant_inventory_entry_id = arg2;
		po.price = arg3;
		po.status = POStatus.PENDING;
		po.rental_period = arg4;
		
		return po;
		
	}
}
