package com.rentit.sales.domain.enumerations;

public enum POStatus {
	PENDING, OPEN, REJECTED, CLOSED
}
