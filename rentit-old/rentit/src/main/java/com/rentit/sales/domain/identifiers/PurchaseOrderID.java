package com.rentit.sales.domain.identifiers;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

@Embeddable
@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class PurchaseOrderID implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8307821144343933353L;
	Long Id;
}
