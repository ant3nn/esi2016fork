package com.rentit.sales.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rentit.sales.domain.identifiers.PurchaseOrderID;
import com.rentit.sales.domain.entities.PurchaseOrder;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, PurchaseOrderID>{
	
	@Query("select p from PurchaseOrder p where p.id = ?1")
	PurchaseOrder selectPlantInventoryEntry(PurchaseOrderID id);

}
