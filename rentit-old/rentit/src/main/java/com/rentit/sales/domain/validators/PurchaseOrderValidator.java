package com.rentit.sales.domain.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.rentit.common.domain.validators.PeriodValidator;
import com.rentit.sales.domain.enumerations.POStatus;
import com.rentit.sales.domain.entities.PurchaseOrder;

public class PurchaseOrderValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object object, Errors errors) {
		
		PurchaseOrder po = (PurchaseOrder) object;
		PeriodValidator pv = new PeriodValidator();
		
		if(po.getId() == null) {
			errors.rejectValue("Id", "PurchaseOrderId cannot be null");
		}

		if(po.getPrice().intValue() < 0) {
			errors.rejectValue("total", "Total cannot be a negative value");
		}
	
		if(po.getStatus().equals(POStatus.PENDING)) {
			
			if(po.getPlant_reservation_id() == null) {
				errors.rejectValue("reservationID", "ReservationID cannot be null");
			}
		}
		
		errors.pushNestedPath("rental_period");
		ValidationUtils.invokeValidator(pv, po.getRental_period(), errors);
		errors.popNestedPath();
	}
}
