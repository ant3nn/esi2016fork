package com.rentit.sales.web;

import com.rentit.inventory.domain.entities.PlantInventoryEntry;
import com.rentit.inventory.domain.repositories.PlantInventoryEntryRepository;
import com.rentit.sales.web.dto.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/dashboard")
public class SalesController {

	@Autowired
    PlantInventoryEntryRepository pier;
	
    @RequestMapping(method = RequestMethod.GET, value="/catalog/form")
    public String getForm(Model model) {
        model.addAttribute("catalogQuery", new PurchaseOrderDTO());
        
        
       
        return "dashboard/catalog/query-form";
    }

    @RequestMapping(method = RequestMethod.POST, value="/catalog/query")
    public String executeQuery(Model model, PurchaseOrderDTO query) {
        System.out.println(query);
        
        List<PlantInventoryEntry> plants = pier.findAvailablePlants(query.getName(), query.getRental_period().getStart_date(), query.getRental_period().getEnd_date());
        
        System.out.println(plants.size());
        
        model.addAttribute("plants", plants);
        
        return "dashboard/catalog/query-result";
    }

}
