package com.rentit.sales.web.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.rentit.inventory.domain.entities.PlantInventoryEntry;
import com.rentit.inventory.domain.repositories.PlantInventoryEntryRepository;
import com.rentit.sales.domain.entities.PurchaseOrder;


public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {
	
	@Autowired
	PlantInventoryEntryRepository plant_inventory_entry_repository;
	
	public PurchaseOrderAssembler(Class<?> controllerClass, Class<PurchaseOrderDTO> resourceType) {
		super(controllerClass, resourceType);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public PurchaseOrderDTO toResource(PurchaseOrder po) {
		PlantInventoryEntry pie = plant_inventory_entry_repository.selectPlantInventoryEntry(po.getPlant_inventory_entry_id());
		
		PurchaseOrderDTO po_dto = createResourceWithId(po.getId().getId(), po);
		
		String plant_inventory_name = pie.getName();
		String plant_inventory_entry_description = pie.getDescription();
		
		po_dto.set_id(po.getId().getId());
		po_dto.setName(plant_inventory_name);
		po_dto.setDescription(plant_inventory_entry_description);
		po_dto.setPrice(po.getPrice());
		
		return po_dto;
	}
	
}
