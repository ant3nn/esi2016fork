package com.rentit.sales.web.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.hateoas.ResourceSupport;

import com.rentit.common.domain.entities.BusinessPeriod;
import com.rentit.sales.domain.enumerations.POStatus;

import lombok.Data;

@Data
public class PurchaseOrderDTO extends ResourceSupport {
	private Long _id;
	//Rain added this
	private String name;
	private String description;
	private LocalDate issue_date;
	private LocalDate  payment_schedule;
	private BigDecimal price;
	private POStatus status;
	private BusinessPeriod rental_period;		
	
}
