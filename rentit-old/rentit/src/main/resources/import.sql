insert into plant_inventory_entry (id, name, description, price) values (1, 'Mini excavator', '1.5 Tonne Mini excavator', 150);
insert into plant_inventory_entry (id, name, description, price) values (2, 'Mini excavator', '3 Tonne Mini excavator', 200);
insert into plant_inventory_entry (id, name, description, price) values (3, 'Midi excavator', '5 Tonne Midi excavator', 250);
insert into plant_inventory_entry (id, name, description, price) values (4, 'Midi excavator', '8 Tonne Midi excavator', 300);
insert into plant_inventory_entry (id, name, description, price) values (5, 'Maxi excavator', '15 Tonne Large excavator', 400);
insert into plant_inventory_entry (id, name, description, price) values (6, 'Maxi excavator', '20 Tonne Large excavator', 450);
insert into plant_inventory_entry (id, name, description, price) values (7, 'HS dumper', '1.5 Tonne Hi-Swivel Dumper', 150);
insert into plant_inventory_entry (id, name, description, price) values (8, 'FT dumper', '2 Tonne Front Tip Dumper', 180);
insert into plant_inventory_entry (id, name, description, price) values (9, 'FT dumper', '2 Tonne Front Tip Dumper', 200);
insert into plant_inventory_entry (id, name, description, price) values (10, 'FT dumper', '2 Tonne Front Tip Dumper', 300);
insert into plant_inventory_entry (id, name, description, price) values (11, 'FT dumper', '3 Tonne Front Tip Dumper', 400);
insert into plant_inventory_entry (id, name, description, price) values (12, 'Loader', 'Hewden Backhoe Loader', 200);
insert into plant_inventory_entry (id, name, description, price) values (13, 'D-Truck', '15 Tonne Articulating Dump Truck', 250);
insert into plant_inventory_entry (id, name, description, price) values (14, 'D-Truck', '30 Tonne Articulating Dump Truck', 300);

ALTER TABLE purchase_order Alter Column start_date VARCHAR(255);
ALTER TABLE purchase_order Alter Column end_date VARCHAR(255);
insert into purchase_order (id, plant_reservation_id, start_date, end_date, price, status) values (1, 2, '2016-03-13', '2016-03-10', 600, 'PENDING');
insert into purchase_order (id, plant_reservation_id, start_date, end_date, price, status) values (2, 2, '2016-03-26', '2016-03-28', 600, 'OPEN');
insert into purchase_order (id, plant_reservation_id, start_date, end_date, price, status) values (4, 2, '2015-03-30', '2015-04-30', 1200, 'REJECTED');
insert into purchase_order (id, plant_reservation_id, start_date, end_date, price, status) values (5, 2, '2016-03-28', '2016-03-30', 600, 'CLOSED');
insert into purchase_order (id, plant_reservation_id, start_date, end_date, price, status) values (7, 2, '2016-03-1', '2016-03-05', 600, 'PENDING');


insert into plant_inventory_item (id, serial_number, condition) values (1, 0001, 'SERVICEABLE');
insert into plant_inventory_item (id, serial_number, condition) values (2, 0002, 'UNSERVICEABLEREPAIRABLE');
insert into plant_inventory_item (id, serial_number, condition) values (3, 0003, 'UNSERVICEABLEREPAIRABLE');
insert into plant_inventory_item (id, serial_number, condition) values (4, 0004, 'UNSERVICEABLEINCOMPLETE');
insert into plant_inventory_item (id, serial_number, condition) values (5, 0005, 'UNSERVICEABLECONDEMNED');

ALTER TABLE plant_reservation Alter Column start_date DATE;
ALTER TABLE plant_reservation Alter Column end_date DATE;
insert into plant_reservation (id, start_date, end_date) values (1, '2016-01-01', '2016-01-02');
insert into plant_reservation (id, start_date, end_date) values (2, '2015-01-01', '2015-01-02');
insert into plant_reservation (id, start_date, end_date) values (3, '2014-01-01', '2014-01-02');
insert into plant_reservation (id, start_date, end_date) values (4, '2013-01-01', '2013-01-02');
insert into plant_reservation (id, start_date, end_date) values (5, '2012-01-01', '2012-01-02');


ALTER TABLE maintenance_task Alter Column start_date DATE;
ALTER TABLE maintenance_task Alter Column end_date DATE;
insert into maintenance_task (id, description, Work, price, start_date, end_date) values (1, '2 Tonne Mini excavator', 'Corrective', 100, '2016-01-01', '2016-01-02');
insert into maintenance_task (id, description, Work, price, start_date, end_date) values (2, '2 Tonne Mini excavator', 'Corrective', 100, '2015-01-01', '2015-01-02');
insert into maintenance_task (id, description, Work, price, start_date, end_date) values (3, '2 Tonne Mini excavator', 'Corrective', 100, '2014-01-01', '2014-01-02');
insert into maintenance_task (id, description, Work, price, start_date, end_date) values (4, '2 Tonne Mini excavator', 'Corrective', 100,  '2013-01-01', '2013-01-02');
insert into maintenance_task (id, description, Work, price, start_date, end_date) values (5, '2 Tonne Mini excavator', 'Corrective', 100, '2012-01-01', '2012-01-02');

insert into maintenance_plan (id, year_of_action) values (1, '2016');
insert into maintenance_plan (id, year_of_action) values (2, '2016');
insert into maintenance_plan (id, year_of_action) values (3, '2016');
insert into maintenance_plan (id, year_of_action) values (4, '2016');
insert into maintenance_plan (id, year_of_action) values (5, '2016');