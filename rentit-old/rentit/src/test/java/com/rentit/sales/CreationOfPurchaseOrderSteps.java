package com.rentit.sales;

import java.math.BigDecimal;
import java.util.List;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.web.context.WebApplicationContext;

import com.rentit.inventory.domain.entities.PlantInventoryEntry;
import com.rentit.inventory.domain.repositories.PlantInventoryEntryRepository;
import com.rentit.sales.domain.repositories.PurchaseOrderRepository;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlDateInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.rentit.RentitApplication;

import cucumber.api.*;
import cucumber.api.java.*;
import cucumber.api.java.en.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RentitApplication.class)
@WebAppConfiguration
public class CreationOfPurchaseOrderSteps {

	@Autowired
	private WebApplicationContext wac;

	private WebClient customerBrowser;
	HtmlPage customerPage;

	@Autowired
	PlantInventoryEntryRepository plantRepo;
	@Autowired
	PurchaseOrderRepository poRepo;

	@Before
	public void setUp() {
		customerBrowser = MockMvcWebClientBuilder.webAppContextSetup(wac).build();
	}

	@After
	public void tearOff() {
		poRepo.deleteAll();
		plantRepo.deleteAll();
	}

	@Given("^the following plants are currently available for rental$")
	public void the_following_plants_are_currently_available_for_rental(List<PlantInventoryEntry> plants)
			throws Throwable {
		plantRepo.save(plants);
	}

	@Given("^a customer is in the \"([^\"]*)\" web page$")
	public void a_customer_is_in_the_web_page(String pageTitle) throws Throwable {
		customerPage = customerBrowser.getPage("http://localhost/dashboard/catalog/form");
	}

	@Given("^No Plant Hire Request exists in the system$")
	public void no_Plant_Hire_Request_exists_in_the_system() throws Throwable {
	}

	@When("^the customer queries the plant catalog for an \"([^\"]*)\" available from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void the_customer_queries_the_plant_catalog_for_an_available_from_to(String plantName, String startDate,
			String endDate) throws Throwable {
		// The following elements are selected by their identifier
		HtmlTextInput nameInput = (HtmlTextInput) customerPage.getElementById("name");
		HtmlDateInput startDateInput = (HtmlDateInput) customerPage.getElementById("rental-start-date");
		HtmlDateInput endDateInput = (HtmlDateInput) customerPage.getElementById("rental-end-date");
		HtmlButton submit = (HtmlButton) customerPage.getElementById("submit-button");

		nameInput.setValueAttribute(plantName);
		startDateInput.setValueAttribute(startDate);
		endDateInput.setValueAttribute(endDate);

		customerPage = submit.click();
	}

	@Then("^(\\d+) plants are shown$")
	public void plants_are_shown(int numberOfPlants) throws Throwable {
		List<?> rows = customerPage.getByXPath("//tr[contains(@class, 'table-row')]");
		// Complete this step and do not forget to remove the following line
		throw new PendingException();
	}

	@When("^the customer selects a \"([^\"]*)\"$")
	public void the_customer_selects_a(String plantDescription) throws Throwable {
		List<?> buttons = customerPage.getByXPath(String.format("//tr[./td = '%s']//button", plantDescription));
		throw new PendingException();
	}

	@Then("^a purchase order should be created with a total price of (\\d+\\.\\d+)$")
	public void a_purchase_order_should_be_created_with_a_total_price_of(BigDecimal total) throws Throwable {
		// Complete this step and do not forget to remove the following line
		throw new PendingException();
	}

}
