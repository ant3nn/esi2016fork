package com.rentit.sales.web;

import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.rentit.RentitApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RentitApplication.class)
@WebAppConfiguration
@DirtiesContext
public class SalesRESTControllerTests {
	
//	@Autowired
//	PlantInventoryEntryRepository repo;
//	
//	@Autowired
//	private WebApplicationContext wac;
//	
//	private MockMvc mockMvc;
//	
//	@Autowired
//	ObjectMapper mapper;
//
//	@Before
//	public void setup() {
//		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
//	}
//
//	@Test
//	@Sql({"schema.sql", "import.sql"})
//	public void testGetAllPlants() throws Exception {
//		MvcResult result = mockMvc.perform(get("/api/sales/plants?name=Exc&startDate=2016-03-14&endDate=2016-03-25"))
//				.andExpect(status().isOk())
//				.andExpect(header().string("Location", isEmptyOrNullString()))
//				.andReturn();
//
//		List<PlantInventoryEntryDTO> plants = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() { });
//
//		assertThat(plants.size(), is(14));
//
//		PurchaseOrderDTO order = new PurchaseOrderDTO();
//		order.setPlant(plants.get(2));
//		order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now()));
//
//		mockMvc.perform(post("/api/sales/orders").content(mapper.writeValueAsString(order)).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isCreated());
//	}
	
}
