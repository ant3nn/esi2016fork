package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QContactPerson is a Querydsl query type for ContactPerson
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QContactPerson extends EntityPathBase<ContactPerson> {

    private static final long serialVersionUID = -595701479L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QContactPerson contactPerson = new QContactPerson("contactPerson");

    public final QContactPersonID id;

    public final StringPath name = createString("name");

    public QContactPerson(String variable) {
        this(ContactPerson.class, forVariable(variable), INITS);
    }

    public QContactPerson(Path<? extends ContactPerson> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QContactPerson(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QContactPerson(PathMetadata<?> metadata, PathInits inits) {
        this(ContactPerson.class, metadata, inits);
    }

    public QContactPerson(Class<? extends ContactPerson> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QContactPersonID(forProperty("id")) : null;
    }

}

