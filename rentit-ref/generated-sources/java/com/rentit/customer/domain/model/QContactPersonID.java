package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QContactPersonID is a Querydsl query type for ContactPersonID
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QContactPersonID extends BeanPath<ContactPersonID> {

    private static final long serialVersionUID = -1238468620L;

    public static final QContactPersonID contactPersonID = new QContactPersonID("contactPersonID");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QContactPersonID(String variable) {
        super(ContactPersonID.class, forVariable(variable));
    }

    public QContactPersonID(Path<? extends ContactPersonID> path) {
        super(path.getType(), path.getMetadata());
    }

    public QContactPersonID(PathMetadata<?> metadata) {
        super(ContactPersonID.class, metadata);
    }

}

