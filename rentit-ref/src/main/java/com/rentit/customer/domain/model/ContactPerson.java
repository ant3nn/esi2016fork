package com.rentit.customer.domain.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Umesh on 11/03/2016.
 */
@Entity
@Data
public class ContactPerson {
    @Id
    @EmbeddedId
    ContactPersonID id;

    String name;
}
