package com.rentit.sales.application.service;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.common.domain.validation.BusinessPeriodValidator;
import com.rentit.inventory.application.dto.PlantReservationDTO;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.inventory.domain.model.PlantReservationID;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.POStatus;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import com.rentit.sales.domain.validation.ContactPersonValidator;
import com.rentit.sales.domain.validation.PurchaseOrderValidator;
import com.rentit.sales.infrastructure.idgeneration.SalesIdentifierGenerator;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.DataBinder;

@Service
public class SalesService {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    SalesIdentifierGenerator identifierGenerator;

    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) throws BindException, PlantNotFoundException {
        PlantInventoryEntry plant = inventoryService.findPlant(purchaseOrderDTO.getPlant());
        PurchaseOrder po = PurchaseOrder.of(
                identifierGenerator.nextPurchaseOrderID(),
                plant.getId(),
                BusinessPeriod.of(purchaseOrderDTO.getRentalPeriod().getStartDate(), purchaseOrderDTO.getRentalPeriod().getEndDate())
        );

        DataBinder binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator(new BusinessPeriodValidator(), new ContactPersonValidator()));
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new BindException(binder.getBindingResult());

        purchaseOrderRepository.save(po);

        PlantReservationDTO reservationDTO = inventoryService.createPlantReservation(purchaseOrderDTO.getPlant(), purchaseOrderDTO.getRentalPeriod());

        po.confirmReservation(
                PlantReservationID.of(reservationDTO.get_id()),
                plant.getPrice()); // Shouldn't we also pass the reservation schedule as a parameter to check if it matches rental period?

        binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator(new BusinessPeriodValidator(), new ContactPersonValidator()));
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new BindException(binder.getBindingResult());

        purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO findPurchaseOrder(PurchaseOrderID id) {
        return purchaseOrderAssembler.toResource(purchaseOrderRepository.findOne(id));
    }

    public PurchaseOrderDTO resubmitPurchaseOrder(PurchaseOrderID id) {

        // I bet it's wrong
        PurchaseOrder po = purchaseOrderRepository.findOne(id);

        return purchaseOrderAssembler.toResource(po);

    }

    public PurchaseOrderDTO closePurchaseOrder(PurchaseOrderID id) {
        return changePurchaseOrderStatus(id, POStatus.CLOSED);
    }

    public PurchaseOrderDTO acceptPurchaseOrder(PurchaseOrderID id) {
        return changePurchaseOrderStatus(id, POStatus.OPEN);
    }

    public PurchaseOrderDTO rejectPurchaseOrder(PurchaseOrderID id) {
        return changePurchaseOrderStatus(id, POStatus.REJECTED);
    }

    private PurchaseOrderDTO changePurchaseOrderStatus(PurchaseOrderID id, POStatus postatus) {
        PurchaseOrder po = purchaseOrderRepository.findOne(id);
        if (po != null) {
            try {
                po.setStatus(postatus);
                purchaseOrderRepository.save(po);
            } catch (Exception e) {
                return null;
            }
            //Return PurchaseOrderDTO after assembling
            return purchaseOrderAssembler.toResource(po);
        } else {
            return null;
        }
    }

    public PurchaseOrderDTO extendRentalPeriod(PurchaseOrderID id) {
        return null;
    }

    public PurchaseOrderDTO acceptRentalPeriodExtension(PurchaseOrderID id, Long extension_id) {
        return null;
    }

    public PurchaseOrderDTO rejectRentalPeriodExtension(PurchaseOrderID id, Long extension_id) {
        return null;
    }
}
