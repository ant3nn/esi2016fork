package com.rentit.sales.rest;

import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.PurchaseOrderID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/sales/orders")
public class PurchaseOrderRestController {
    @Autowired
    SalesService salesService;

    @RequestMapping(method = POST, path = "")
    public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody PurchaseOrderDTO poDTO) throws Exception {
        poDTO = salesService.createPurchaseOrder(poDTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(poDTO.getId().getHref()));
        return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = GET, path = "/{id}")
    public PurchaseOrderDTO showPurchaseOrder(@PathVariable Long id) throws Exception {
        PurchaseOrderDTO poDTO = salesService.findPurchaseOrder(PurchaseOrderID.of(id));
        return poDTO;
    }

    @RequestMapping(method = PUT, path = "/{id}")
    public PurchaseOrderDTO resubmitPurchaseOrder(@PathVariable Long id) throws Exception {
        return salesService.resubmitPurchaseOrder(PurchaseOrderID.of(id));

    }

    @RequestMapping(method = DELETE, path = "/{id}")
    public PurchaseOrderDTO closePurchaseOrder(@PathVariable Long id) throws Exception {
        return salesService.closePurchaseOrder(PurchaseOrderID.of(id));
    }

    @RequestMapping(method = POST, path = "/{id}/accept")
    public PurchaseOrderDTO acceptPurchaseOrder(@PathVariable Long id) throws Exception {
        return salesService.acceptPurchaseOrder(PurchaseOrderID.of(id));
    }

    @RequestMapping(method = DELETE, path = "/{id}/accept")
    public PurchaseOrderDTO rejectPurchaseOrder(@PathVariable Long id) throws Exception {
        return salesService.rejectPurchaseOrder(PurchaseOrderID.of(id));
    }

    @RequestMapping(method = POST, path = "/{id}/extensions")
    public PurchaseOrderDTO extendRentalPeriod(@PathVariable Long id) throws Exception {
        return salesService.extendRentalPeriod(PurchaseOrderID.of(id));
    }

    @RequestMapping(method = POST, path = "/{id}/extensions/{eid}/accept")
    public PurchaseOrderDTO acceptRentalPeriodExtension(@PathVariable Long id,@PathVariable Long eid) throws Exception {
        return null;
    }

    @RequestMapping(method = DELETE, path = "/{id}/extensions/{eid}/accept")
    public PurchaseOrderDTO rejectRentalPeriodExtension(@PathVariable Long id, @PathVariable Long eid) throws Exception {
        return null;
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public String bindExceptionHandler(Exception ex) {
        return ex.getMessage();
    }
}
